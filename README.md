# fifo-omnibus

An all-in-one setup script to bootstrap FiFo based clouds

> This is not an officially supported FiFo project

## Usage

```bash
sh -c $(curl -k -L https://gitlab.com/lbivens/fifo-omnibus/-/raw/master/fifo_omnibus.sh)
```

## Custom Settings
Script parameters can be overwritten by environment variables:

| Env. Var.  | Script Parameter | 
|------------|--------------------|
| FIFOVER    | FiFo Version  |
| FIFOBRANCH | FiFo Branch   |
| FIFODSET   | FiFo AIO Dataset UUID |
| FIFPBASE   | FiFo Base Dataset UUID |
| FIFOADDR   | FiFo zone IP Addr |
| FIFOIORG   | FiFo administrator organization |
| FIFOS3     | FiFo S3 compliant storage, if any |

