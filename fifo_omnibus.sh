#!/bin/bash

# All in one fifo installer

fifo_version="${FIFOVER:-0.9.3}"
branch="${FIFOBRANCH:-rel}"

aio_dataset="${FIFODSET:-b67e58ba-b4f1-4a99-98d4-0df132a57343}"
base_dataset="${FIFPBASE:-2f538996-672b-11e7-af09-2331e56e26e9}"

fifo_zone_ip="${FIFOADDR:-10.0.0.10}"
fifo_org="${FIFOIORG:-fifo}"
fifo_storage_host="${FIFOS3:-no_s3}"



# Default settings
nic_tag='admin'
fifo_gw=$(route -n get default | awk '/gateway/{print $2}')

# Sets the same dns as the global zone 
fifo_dns=$(grep dns_resolvers /usbkey/config | cut -d= -f2 | sed -e "s/\(.*\)/\"\1\"/" -e "s/,/\",\"/") 

get_dataset()
{
imgadm import ${dataset} > /dev/null
if [ ! $? -eq 0 ]; then
  printf "Adding FiFo dataset sources...\n"
  imgadm sources -a https://datasets.project-fifo.net
  imgadm update
  printf "Fetching latest dataset...\n"
  dataset=$(imgadm avail -o name,version,uuid | awk '/^fifo-aio.*${fifo_version}/{printf $3}')
  imgadm import ${dataset}
fi
}

create_fifo_manifest ()
{
printf "Generating FiFo zone manifest...\n"
cat > /var/tmp/fifo.json << EOF
{
 "autoboot": true,
 "brand": "joyent",
 "image_uuid": "${dataset}",
 "delegate_dataset": true,
 "indestructible_delegated": true,
 "max_physical_memory": 3072,
 "cpu_cap": 100,
 "alias": "fifo",
 "quota": "40",
 "resolvers": [${fifo_dns}],
 "nics": [
  {
   "interface": "net0",
   "nic_tag": "${nic_tag}",
   "ip": "${fifo_zone_ip}",
   "gateway": "${fifo_gw}",
   "netmask": "255.255.255.0"
  }
 ]
}
EOF
}



create_leofs_manager_manifest() {
printf "Generating LeoFS Manager zone manifest...\n"
cat > /var/tmp/leofs_manager.json << EOF
{
 "autoboot": true,
 "brand": "joyent",
 "image_uuid": "e1faace4-e19b-11e5-928b-83849e2fd94a",
 "delegate_dataset": true,
 "max_physical_memory": 512,
 "cpu_cap": 100,
 "alias": "leofs",
 "quota": "20",
 "resolvers": [
  "10.0.100.1"
 ],
 "nics": [
  {
   "interface": "net0",
   "nic_tag": "admin",
   "ip": "10.0.100.52",
   "gateway": "10.0.100.1",
   "netmask": "255.255.255.0"
  }
 ]
}
EOF
}


start_fifo()
{
printf "Configuring fifo...\n"
zlogin ${zone_uuid} 'fifo-config' 

printf "Starting services...\n"
zone_services=(epmd snarl howl sniffle)
for service in ${zone_services[@]}; do
  zlogin ${zone_uuid} svcs ${service} | grep online > /dev/null
  if [ ! ${?} -eq 0 ]; then
    printf "\t - ${service}\n"
    zlogin ${zone_uuid} svcadm enable ${service}
    sleep 2
  fi
done

printf "Waiting for all services to be active...\n"
zone_ports=(4200 4210)
for port in ${zone_ports[@]}; do
  until zlogin ${zone_uuid} nc -w 2 -v ${fifo_zone_ip} ${port}; do
    sleep 5
  done
done

printf "Setting up permissions...\n"
zlogin ${zone_uuid} "snarl-admin init default ${fifo_org} Users admin admin"
printf "Setting up storage...\n"
zlogin ${zone_uuid} "sniffle-admin config set storage.s3.host ${fifo_storage_host}"
printf "Setting up additional dataset sources..."
zlogin ${zone_uuid} "sniffle-admin datasets servers add https://datasets.at/images"
}

install_zdoor() 
{
printf "Installing zdoor...\n"
curl -s -o /opt/fifo_zlogin-latest.gz http://release.project-fifo.net/gz/${branch}/fifo_zlogin-latest.gz 
gunzip /opt/fifo_zlogin-latest.gz 
sh /opt/fifo_zlogin-latest

}

install_chunter()
{
printf "Installing chunter...\n"
curl -s -o /opt/chunter-latest.gz http://release.project-fifo.net/gz/${branch}/chunter-latest.gz
gunzip /opt/chunter-latest.gz
sh /opt/chunter-latest
}

printf "//////////////////////\n"
printf "FiFo Omnibus Installer\n\n"
printf "!!! This is not an official Project FiFo installer\n"
printf "!!! For support contact dev@null.de\n"
printf "!!! This installer is built and tested exclusively for version %s\n\n" ${fifo_version}

if [ $(uname) = 'SunOS' ]; then
  case ${1} in
    "all")
          printf "> Bootstraping all\n"
          dataset=${aio_dataset}
          get_dataset
          create_fifo_manifest 
          if [ $(vmadm list alias=fifo -o uuid -H | wc -l) -eq 0 ]; then
            vmadm create -f /var/tmp/fifo.json
          else
            printf "A FiFo zone is already running\n"
            exit 1
          fi
          zone_uuid=$(vmadm list alias=fifo -o uuid -H)
          start_fifo
          install_zdoor
          install_chunter
          ;;
    "zone")
          printf "> Bootstraping FiFo Zone only\n"
          dataset=${base_dataset}
          get_dataset
          create_fifo_manifest 
          printf "Starting zone...\n"
          if [ $(vmadm list alias=fifo -o uuid -H | wc -l) -eq 0 ]; then
            vmadm create -f /var/tmp/fifo.json
          else
            printf "A FiFo zone is already running\n"
            exit 1
          fi
          zone_uuid=$(vmadm list alias=fifo -o uuid -H)
          printf "Setting up FiFo packages..."
          zlogin ${zone_uuid} "zfs set mountpoint=/data zones/${zone_uuid}/data"
          zlogin ${zone_uuid} "curl -L https://project-fifo.net/fifo.gpg -o /data/fifo.gpg"
          zlogin ${zone_uuid} "gpg --primary-keyring /opt/local/etc/gnupg/pkgsrc.gpg --import < /data/fifo.gpg"
          zlogin ${zone_uuid} "gpg --keyring /opt/local/etc/gnupg/pkgsrc.gpg --fingerprint"
          zlogin ${zone_uuid} 'echo "http://release.project-fifo.net/pkg/17.2.0/rel" >> /opt/local/etc/pkgin/repositories.conf'
          zlogin ${zone_uuid} "pkgin -fy up"
          zlogin ${zone_uuid} "pkgin install -fy fifo-snarl fifo-sniffle fifo-howl fifo-cerberus"
          start_fifo
          ;;
    "chunter")
          install_zdoor
          install_chunter
          ;;
    "leofs")
          printf "> Installing LeoFS\n"
          ;;
    *) 
      printf "Usage: you should really know what up\n"
      ;;
  esac

  exit 0
else
  printf "At the moment Project FiFo only works on SmartOS, OmniOS, and Solaris"
  exit 1
fi

